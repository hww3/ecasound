import Public.Audio.ECASound;

#define OUTPORTS 8
#define OUTPORTNAME "system:playback_"
#define OUTSTART 1
#define FPS 30
#define AUDIOLOOP "classic.wav"

Client client;
Chainsetup cs;
Chain chain;
object EQ;
object Enhancer;
object Comp;
object Limiter;
object XOver;

array(mapping(string:mixed)) subscriptions;

int running = 0;

int main()
{
    init();
    return -1;
}

void init () {
    client = Client();
	// setup and chain names are somewhat arbitrary.
	// a longer term goal would be to allow multiple setups to be present at once and selectable by name.
	// you add a chain to the setup per set of channels. by default there are 2 channels for stereo, etc.
    cs = Chainsetup("freqmaster");
    chain = Chain("dsp");
    
    // input can be a path to an audio file, a jack or alsa specification, or can be null
    chain->set_input(AUDIOLOOP);
    
    setup();
    
    array out = ({"jack_multi"});
    for (int i = OUTSTART; i <= OUTPORTS; i++)
        out += ({OUTPORTNAME+i});
    
    chain->set_output(out * ",");
    
    cs->add_chain(chain);
    client->enable_chainsetup(cs);
    
    start();
    
    call_out(experiments, 1);
}

void setup () {
    EQ = Equalizer8Band();
    chain->add_operation(EQ);
    
    Enhancer = MultibandEnhancer();
    chain->add_operation(Enhancer);
    
    Comp = MultibandCompressor();
    chain->add_operation(Comp);
    
    Limiter = MultibandLimiter();
    chain->add_operation(Limiter);
    
    XOver = XOver3Band();
    chain->add_operation(XOver);
    
    subscriptions += ({
        ([ "operator":Comp, "param":"compression0" ]),
        ([ "operator":Comp, "param":"compression1" ]),
        ([ "operator":Comp, "param":"compression2" ]),
        ([ "operator":Limiter, "param":"att0" ]),
        ([ "operator":Limiter, "param":"att1" ]),
        ([ "operator":Limiter, "param":"att2" ]),
        ([ "operator":XOver, "param":"meter_L1" ]),
        ([ "operator":XOver, "param":"meter_R1" ]),
        ([ "operator":XOver, "param":"meter_L2" ]),
        ([ "operator":XOver, "param":"meter_R2" ]),
        ([ "operator":XOver, "param":"meter_L3" ]),
        ([ "operator":XOver, "param":"meter_R3" ])
    });
}

void start () {
    // required for audio files, probably not for live data.
    running = 1;
    client->set_position(0.0);	
    client->start();
    continuous();
}
void stop () {
    client->stop();
    running = 0;
}
void finish () {
    stop();
    exit(0);
}

void experiments() {
    EQ->set("p3_active", 1);
    EQ->set("p3_level", 8);
}

void continuous () {
    if (!running)
        return;
    foreach(subscriptions;;mapping s) {
        write("%s %O\n", s["param"], s["operator"]->get(s["param"]));
    }
    call_out(continuous, 1.0/FPS);
}


