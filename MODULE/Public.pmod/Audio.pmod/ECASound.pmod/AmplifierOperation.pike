inherit .Operation;

apping(string:mixed) description = ([
    "_audio": ({
        ([
          "index": 0,
          "io": "input",
          "name": "In L",
          "symbol": "in_l",
          "type": "audio"
        ]),
        ([
          "index": 1,
          "io": "input",
          "name": "In R",
          "symbol": "in_r",
          "type": "audio"
        ]),
        ([
          "index": 2,
          "io": "output",
          "name": "Out L",
          "symbol": "out_l",
          "type": "audio"
        ]),
        ([
          "index": 3,
          "io": "output",
          "name": "Out R",
          "symbol": "out_r",
          "type": "audio"
        ])
    }),
    "_ports": ({
        ([
          "index": 0,
          "io": "input",
          "name": "In L",
          "symbol": "in_l",
          "type": "audio"
        ]),
        ([
          "index": 1,
          "io": "input",
          "name": "In R",
          "symbol": "in_r",
          "type": "audio"
        ]),
        ([
          "index": 2,
          "io": "output",
          "name": "Out L",
          "symbol": "out_l",
          "type": "audio"
        ]),
        ([
          "index": 3,
          "io": "output",
          "name": "Out R",
          "symbol": "out_r",
          "type": "audio"
        ]),
        ([
          "controllable": 1,
          "default": 1.0,
          "index": 4,
          "io": "input",
          "maximum": 100.0,
          "minimum": 0.0,
          "name": "Gain",
          "symbol": "gain",
          "type": "float"
        ])
    }),
    "_param": ({ 
        ([
          "controllable": 1,
          "default": 1.0,
          "index": 4,
          "io": "input",
          "maximum": 100.0,
          "minimum": 0.0,
          "name": "Gain",
          "symbol": "gain",
          "type": "float"
        ])
    }),
    
    "audio": ([
        "in_l": ([
          "index": 0,
          "io": "input",
          "name": "In L",
          "symbol": "in_l",
          "type": "audio"
        ]),
        "in_r": ([
          "index": 1,
          "io": "input",
          "name": "In R",
          "symbol": "in_r",
          "type": "audio"
        ]),
        "out_l": ([
          "index": 2,
          "io": "output",
          "name": "Out L",
          "symbol": "out_l",
          "type": "audio"
        ]),
        "out_r": ([
          "index": 3,
          "io": "output",
          "name": "Out R",
          "symbol": "out_r",
          "type": "audio"
        ])
    ]),
    "ports": ([
        "in_l": ([
          "index": 0,
          "io": "input",
          "name": "In L",
          "symbol": "in_l",
          "type": "audio"
        ]),
        "in_r": ([
          "index": 1,
          "io": "input",
          "name": "In R",
          "symbol": "in_r",
          "type": "audio"
        ]),
        "out_l": ([
          "index": 2,
          "io": "output",
          "name": "Out L",
          "symbol": "out_l",
          "type": "audio"
        ]),
        "out_r": ([
          "index": 3,
          "io": "output",
          "name": "Out R",
          "symbol": "out_r",
          "type": "audio"
        ]),
        "gain": ([
          "controllable": 1,
          "default": 1.0,
          "index": 4,
          "io": "input",
          "maximum": 100.0,
          "minimum": 0.0,
          "name": "Gain",
          "symbol": "gain",
          "type": "float"
        ])
    ]),
    "param": ([ 
        "gain": ([
          "controllable": 1,
          "default": 1.0,
          "index": 4,
          "io": "input",
          "maximum": 100.0,
          "minimum": 0.0,
          "name": "Gain",
          "symbol": "gain",
          "type": "float"
        ])
    ]),
    
    "author": "ECAsound",
    "generic": "Amplifier",
    "haslatency": 0,
    "name": "Amplifier",
    "type": "generic",
    "operator": "a"
]);
