
string name;
.Chainsetup setup;
string input_spec;
string output_spec;
string input_format;
string output_format;

array(.Operation) operations = ({});
int ops = 1;
int is_enabled;

void create(string name) {
	this->name = name;
}

//! ECASound input specification
//! May be a valid audio file, alsa:spec, or jack:spec, if ECASound is compiled with support for it.
void set_input(string spec) {
	input_spec = spec;

	if(is_enabled) {
		// ECASound supports multiple input/output objects, but we only handle 1 at a time.
   	  setup->client->stop();
	  
	  string is = input_spec;
      if(input_format)
        is = "resample,auto," + is;
	  setup->client->send_command("ai-add " + is);
	  setup->client->send_chain_command(setup->name, name, "ai-index-select 1");
	  setup->client->send_chain_command(setup->name, name, "ai-remove");
   	  setup->client->start();
	}
}

//! ECASound output specification
//! May be a valid audio file, alsa:spec, or jack:spec, if ECASound is compiled with support for it.
void set_output(string spec) {
	output_spec = spec;
	
	if(is_enabled) {
      setup->client->stop();
		// ECASound supports multiple input/output objects, but we only handle 1 at a time.
  	  string os = output_spec;
        if(output_format)
          os = "resample,auto," + os;
  	  setup->client->send_command("ao-add " + os);

	  setup->client->send_chain_command(setup->name, name, " ao-index-select 1");
	  setup->client->send_chain_command(setup->name, name, "ao-remove");
      setup->client->start();
 	}
}

//! ECASound input specification
void set_input_format(string spec) {
	input_format = spec;
}

//! ECASound output specification
void set_output_format(string spec) {
	output_format = spec;
}

void add_operation(.Operation op) {
	operations += ({op});
}

void enable(.Client client, .Chainsetup setup) {
    this->setup = setup;
    client->send_command("c-add " + name);

    is_enabled = 1;

	string is = input_spec;
	string os = output_spec;
	
	if(!is) is = "null";
	if(!os) os = "null";
	
	if(input_format) {
      is = "resample,auto," + is;
	  client->send_command("cs-option -f:" + input_format);
    }	  
	
    client->send_command("ai-add " + is);

	if(output_format) {
      os = "resample,auto," + os;
      client->send_command("cs-option -f:" + output_format);
    }
  
    client->send_command("ao-add " + os);
	  
    foreach(operations;; .Operation operation) {
		operation->enable(client, this, ops++);
	}

}

void mute() {
	setup->client->send_chain_command(setup->name, name, "c-mute,on");
}

void unmute() {
	setup->client->send_chain_command(setup->name, name, "c-mute,off");
}
