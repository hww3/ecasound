
string name;

array(.Chain) chains = ({});
.Client client;
	
void create(string name) {
	this->name = name;
}

void add_chain(.Chain chain) {
	chains += ({chain});
}

void enable(.Client client) {

	this->client = client;
    client->send_command("cs-add " + name);
  
    foreach(chains;; .Chain chain) {
		chain->enable(client, this);
	}
}
