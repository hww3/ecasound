
#ifdef ECA_DEBUG
#define DEBUG(X...) werror(X)
#else
#define DEBUG(X...)
#endif

object ecasound;
Stdio.File stdout = Stdio.File();
Stdio.File stdin = Stdio.File();
Stdio.File stderr = Stdio.File();
string last_reply;
string last_returntype; 

int recv = 0;
int sent = 0;

.Chainsetup current_cs;

mapping chainsetups = ([]);

int(0..1) initialized = 0;
string current_chain;
int(0..1) has_chainsetup = 0;
int(0..1) running;

variant void create() {
  create("ecasound");
}

protected variant void create(string ecasound_path) {
	werror("ecasound: " + ecasound_path + "\n");
   mapping opts = ([ "stdout" : stdout->pipe(), "stdin": stdin->pipe(), "stderr" : stderr->pipe() ]);
   ecasound = Process.create_process(({ecasound_path, "-c" }), opts );
   int got_prompt = 0;
   do {
     string d = stdout->read(1024, 1);
	 DEBUG("data: %O", d);
	 if(search(d, ">", 0) != -1) got_prompt = 1;
   }
   while(!got_prompt);
	   
//   stdout->set_nonblocking();
//   stdout->set_read_callback(data_recv);

   stderr->set_nonblocking();
   stderr->set_read_callback(edata_recv);
   
//   send_command("debug 256");
   send_command("int-set-float-to-string-precision 17");
   send_command("int-output-mode-wellformed");
   initialized = 1;
}

protected void destroy() {
	if(ecasound){
	  ecasound->kill();
	  ecasound = 0;
    }
}

void enable_chainsetup(.Chainsetup cs) {
  if(has_chainsetup) throw(Error.Generic("Chainsetup " + current_cs->name + " already running.\n"));

  current_cs = cs;  
  enable();
}

void stop() {
	if(!current_cs) throw(Error.Generic("No Chainsetup configured.\n")); 	
	if(!running) throw(Error.Generic("Not running.\n"));
	send_command("stop");
	running = 0;
}

void enable() {
	if(!current_cs) throw(Error.Generic("No Chainsetup configured.\n"));
	if(running) throw(Error.Generic("Already running.\n"));
	current_cs->enable(this);
	has_chainsetup = 1;
	send_command("cs-select " + current_cs->name);
	send_command("cs-connect");
}

void start() {
	if(running) throw(Error.Generic("Already running\n"));
	send_command("start");
	running = 1;
}

void send_command(string command) {
  sent++;
  werror(command + "\n");
  DEBUG("sending command %d: %O\n", sent, command);
  stdin->write(command + "\n");
  read_response();
}

string engine_status() {
    if(!has_chainsetup) throw(Error.Generic("No chainsetup present\n"));
    send_command("engine-status");
    return last_string();
}

float get_position() {
  if(!has_chainsetup) throw(Error.Generic("No chainsetup present\n"));
  send_command("get-position");
  return last_float();
}

void set_position(float pos) {
    if(!has_chainsetup) throw(Error.Generic("No chainsetup present\n"));
    send_command("cs-set-position " + pos);
}

mixed send_setup_command(string setup_name, string command) {
	// TODO verify requested setup is actually in place.	
	send_command(command);
	return 0;
}

mixed send_chain_command(string setup_name, string chain_name, string command) {
	// TODO verify requested setup is actually in place.	
	if(current_chain != chain_name) {
      send_command("c-select " + chain_name);
	  current_chain = chain_name;
    }
	
	send_command(command);
	return 0;
}

mixed send_copp_get_command(string setup_name, string chain_name, int operation_id, int parameter_id) {
	// TODO verify setup_name matches current setup.
	// TODO make reentrant
	// TODO error checking
	if(current_chain != chain_name) {
      send_command("c-select " + chain_name);
	  current_chain = chain_name;
    }
    send_command("cop-get " + operation_id + "," + parameter_id);
	return last_float();
}

mixed send_copp_set_command(string setup_name, string chain_name, int operation_id, int parameter_id, mixed value) {
	// TODO verify setup_name matches current setup.
	// TODO verify setup_name matches current setup.
	// TODO make reentrant
	// TODO error checking
	
	if(current_chain != chain_name) {
      send_command("c-select " + chain_name);
	  current_chain = chain_name;
    }
    send_command("cop-set " + operation_id + "," + parameter_id + "," + value);
	return last_float();
}

void read_response() {
	recv++;
	string data = "";
	int have_result;
	do {
	  string d = stdout->read(1024,1);
	  if(d) data = d;
//	  werror("data: %O\n", data);
	  if(data && data != "") have_result = data_recv(recv, data);
    } while(initialized && !have_result);
}

int data_recv(mixed id, string data) {
// werror("data-> %d: %O\n", recv, data);
 int loglevel, len;
 sscanf(data, "%d %d%s", loglevel, len, data);
 if(sizeof(data) && data[0] == ' ') {
	 last_returntype = data[1..1];
	 
     last_reply = data[4..4+len-1];
	 DEBUG("last_reply: type=%s, data=%O\n", last_returntype, last_reply);
     if(last_returntype == "e") {
		 throw(Error.Generic(last_reply));
	 }
	 
	 return 1;
 }
 else if(sizeof(data) && has_prefix(data, "\r\n"))
 {
     last_reply = data[2..(2+len-1)];
	 return 1;
//	 werror("last_reply: %O\n", last_reply);
 }
 else if(initialized)
	 DEBUG("invalid data: %O\n", data);
 else DEBUG(data);
 return 0;
}

string last_string() {
	string lr = last_reply;
	last_reply = "";
	return lr;
}

float last_float() {
	string lr = last_reply;
	last_reply = "";
	return (float)lr;
}


void edata_recv(mixed id, string data) {

 werror("edata: " + data + "\n");
 
}
