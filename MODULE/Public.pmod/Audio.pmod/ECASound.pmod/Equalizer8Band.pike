inherit .Operation;

mapping(string:mixed) description = ([ /* 12 elements */
  "author": "Calf Studio Gear",
  "authoremail": "mailto:info@calf-studio-gear.org",
  "authorhomepage": "http://calf-studio-gear.org/",
  "binary": "file:///usr/local/lib/lv2/calf.lv2/calflv2gui.so",
  "bundle": "file:///usr/local/lib/lv2/calf.lv2/",
  "generic": "CalfEqualizer8Band",
  "haslatency": 0,
  "name": "Calf Equalizer 8 Band",
  "operator": "lv2",
  "origin": "Equalizer8Band.lv2",
  "ports": ({ /* 53 elements */
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#left",
          "group": "http://calf.sourceforge.net/plugins/Equalizer8Band#in",
          "index": 0,
          "io": "input",
          "name": "In L",
          "symbol": "in_l",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#right",
          "group": "http://calf.sourceforge.net/plugins/Equalizer8Band#in",
          "index": 1,
          "io": "input",
          "name": "In R",
          "symbol": "in_r",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#left",
          "group": "http://calf.sourceforge.net/plugins/Equalizer8Band#out",
          "index": 2,
          "io": "output",
          "name": "Out L",
          "symbol": "out_l",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#right",
          "group": "http://calf.sourceforge.net/plugins/Equalizer8Band#out",
          "index": 3,
          "io": "output",
          "name": "Out R",
          "symbol": "out_r",
          "type": "audio"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 4,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Bypass",
          "symbol": "bypass",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 5,
          "io": "input",
          "maximum": 64.0,
          "minimum": 0.015625,
          "name": "Input Gain",
          "symbol": "level_in",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 6,
          "io": "input",
          "maximum": 64.0,
          "minimum": 0.015625,
          "name": "Output Gain",
          "symbol": "level_out",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 7,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Meter-InL",
          "symbol": "meter_inL",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 8,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Meter-InR",
          "symbol": "meter_inR",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 9,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Meter-OutL",
          "symbol": "meter_outL",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 10,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Meter-OutR",
          "symbol": "meter_outR",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 11,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": 0.0,
          "symbol": "clip_inL",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 12,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": 0.0,
          "symbol": "clip_inR",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 13,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": 0.0,
          "symbol": "clip_outL",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 14,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": 0.0,
          "symbol": "clip_outR",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 0.0,
          "entries": ([ /* 6 elements */
              0: " ",
              1: "ON",
              2: "Left",
              3: "Right",
              4: "Mid",
              5: "Side"
            ]),
          "index": 15,
          "io": "input",
          "maximum": 5.0,
          "minimum": 0.0,
          "name": "HP Active",
          "symbol": "hp_active",
          "type": "enum"
        ]),
        ([ /* 8 elements */
          "default": 30.0,
          "index": 16,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "HP Freq",
          "symbol": "hp_freq",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 1.0,
          "entries": ([ /* 3 elements */
              0: "12dB/oct",
              1: "24dB/oct",
              2: "36dB/oct"
            ]),
          "index": 17,
          "io": "input",
          "maximum": 2.0,
          "minimum": 0.0,
          "name": "HP Mode",
          "symbol": "hp_mode",
          "type": "int"
        ]),
        ([ /* 8 elements */
          "default": 0.707,
          "index": 18,
          "io": "input",
          "maximum": 10.0,
          "minimum": 0.1,
          "name": "HP Q",
          "symbol": "hp_q",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 0.0,
          "entries": ([ /* 6 elements */
              0: " ",
              1: "ON",
              2: "Left",
              3: "Right",
              4: "Mid",
              5: "Side"
            ]),
          "index": 19,
          "io": "input",
          "maximum": 5.0,
          "minimum": 0.0,
          "name": "LP Active",
          "symbol": "lp_active",
          "type": "enum"
        ]),
        ([ /* 8 elements */
          "default": 18000.0,
          "index": 20,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "LP Freq",
          "symbol": "lp_freq",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 1.0,
          "entries": ([ /* 3 elements */
              0: "12dB/oct",
              1: "24dB/oct",
              2: "36dB/oct"
            ]),
          "index": 21,
          "io": "input",
          "maximum": 2.0,
          "minimum": 0.0,
          "name": "LP Mode",
          "symbol": "lp_mode",
          "type": "enum"
        ]),
        ([ /* 8 elements */
          "default": 0.707,
          "index": 22,
          "io": "input",
          "maximum": 10.0,
          "minimum": 0.1,
          "name": "LP Q",
          "symbol": "lp_q",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 0.0,
          "entries": ([ /* 6 elements */
              0: " ",
              1: "ON",
              2: "Left",
              3: "Right",
              4: "Mid",
              5: "Side"
            ]),
          "index": 23,
          "io": "input",
          "maximum": 5.0,
          "minimum": 0.0,
          "name": "LS Active",
          "symbol": "ls_active",
          "type": "enum"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 24,
          "io": "input",
          "maximum": 64.0,
          "minimum": 0.015625,
          "name": "Level L",
          "symbol": "ls_level",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 100.0,
          "index": 25,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "Freq L",
          "symbol": "ls_freq",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.707,
          "index": 26,
          "io": "input",
          "maximum": 10.0,
          "minimum": 0.1,
          "name": "LS Q",
          "symbol": "ls_q",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 0.0,
          "entries": ([ /* 6 elements */
              0: " ",
              1: "ON",
              2: "Left",
              3: "Right",
              4: "Mid",
              5: "Side"
            ]),
          "index": 27,
          "io": "input",
          "maximum": 5.0,
          "minimum": 0.0,
          "name": "HS Active",
          "symbol": "hs_active",
          "type": "enum"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 28,
          "io": "input",
          "maximum": 64.0,
          "minimum": 0.015625,
          "name": "Level H",
          "symbol": "hs_level",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 5000.0,
          "index": 29,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "Freq H",
          "symbol": "hs_freq",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.707,
          "index": 30,
          "io": "input",
          "maximum": 10.0,
          "minimum": 0.1,
          "name": "HS Q",
          "symbol": "hs_q",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 0.0,
          "entries": ([ /* 6 elements */
              0: " ",
              1: "ON",
              2: "Left",
              3: "Right",
              4: "Mid",
              5: "Side"
            ]),
          "index": 31,
          "io": "input",
          "maximum": 5.0,
          "minimum": 0.0,
          "name": "F1 Active",
          "symbol": "p1_active",
          "type": "enum"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 32,
          "io": "input",
          "maximum": 64.0,
          "minimum": 0.015625,
          "name": "Level 1",
          "symbol": "p1_level",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 100.0,
          "index": 33,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "Freq 1",
          "symbol": "p1_freq",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 34,
          "io": "input",
          "maximum": 100.0,
          "minimum": 0.1,
          "name": "Q 1",
          "symbol": "p1_q",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 0.0,
          "entries": ([ /* 6 elements */
              0: " ",
              1: "ON",
              2: "Left",
              3: "Right",
              4: "Mid",
              5: "Side"
            ]),
          "index": 35,
          "io": "input",
          "maximum": 5.0,
          "minimum": 0.0,
          "name": "F2 Active",
          "symbol": "p2_active",
          "type": "enum"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 36,
          "io": "input",
          "maximum": 64.0,
          "minimum": 0.015625,
          "name": "Level 2",
          "symbol": "p2_level",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 500.0,
          "index": 37,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "Freq 2",
          "symbol": "p2_freq",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 38,
          "io": "input",
          "maximum": 100.0,
          "minimum": 0.1,
          "name": "Q 2",
          "symbol": "p2_q",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 0.0,
          "entries": ([ /* 6 elements */
              0: " ",
              1: "ON",
              2: "Left",
              3: "Right",
              4: "Mid",
              5: "Side"
            ]),
          "index": 39,
          "io": "input",
          "maximum": 5.0,
          "minimum": 0.0,
          "name": "F3 Active",
          "symbol": "p3_active",
          "type": "enum"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 40,
          "io": "input",
          "maximum": 64.0,
          "minimum": 0.015625,
          "name": "Level 3",
          "symbol": "p3_level",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 2000.0,
          "index": 41,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "Freq 3",
          "symbol": "p3_freq",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 42,
          "io": "input",
          "maximum": 100.0,
          "minimum": 0.1,
          "name": "Q 3",
          "symbol": "p3_q",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 0.0,
          "entries": ([ /* 6 elements */
              0: " ",
              1: "ON",
              2: "Left",
              3: "Right",
              4: "Mid",
              5: "Side"
            ]),
          "index": 43,
          "io": "input",
          "maximum": 5.0,
          "minimum": 0.0,
          "name": "F4 Active",
          "symbol": "p4_active",
          "type": "enum"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 44,
          "io": "input",
          "maximum": 64.0,
          "minimum": 0.015625,
          "name": "Level 4",
          "symbol": "p4_level",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 5000.0,
          "index": 45,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "Freq 4",
          "symbol": "p4_freq",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 46,
          "io": "input",
          "maximum": 100.0,
          "minimum": 0.1,
          "name": "Q 4",
          "symbol": "p4_q",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 47,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Individual Filters",
          "symbol": "individuals",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.25,
          "index": 48,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0625,
          "name": "Zoom",
          "symbol": "zoom",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 49,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Analyzer Active",
          "symbol": "analyzer",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 1.0,
          "entries": ([ /* 3 elements */
              0: "Input",
              1: "Output",
              2: "Difference"
            ]),
          "index": 50,
          "io": "input",
          "maximum": 2.0,
          "minimum": 0.0,
          "name": "Analyzer Mode",
          "symbol": "analyzer_mode",
          "type": "enum"
        ]),
        ([ /* 5 elements */
          "index": 51,
          "io": "input",
          "name": "Events",
          "symbol": "events_in",
          "type": "atom"
        ]),
        ([ /* 5 elements */
          "index": 52,
          "io": "output",
          "name": "Events",
          "symbol": "events_out",
          "type": "atom"
        ])
    }),
  "uri": "http://calf.sourceforge.net/plugins/Equalizer8Band"
]);
