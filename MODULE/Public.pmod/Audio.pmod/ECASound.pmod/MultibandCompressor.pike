inherit .Operation;

mapping(string:mixed) description = ([ /* 12 elements */
  "author": "Calf Studio Gear",
  "authoremail": "mailto:info@calf-studio-gear.org",
  "authorhomepage": "http://calf-studio-gear.org/",
  "binary": "file:///usr/local/lib/lv2/calf.lv2/calflv2gui.so",
  "bundle": "file:///usr/local/lib/lv2/calf.lv2/",
  "generic": "CalfMultibandCompressor",
  "haslatency": 0,
  "name": "Calf Multiband Compressor",
  "operator": "lv2",
  "origin": "MultibandCompressor.lv2",
  "ports": ({ /* 66 elements */
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#left",
          "group": "http://calf.sourceforge.net/plugins/MultibandCompressor#in",
          "index": 0,
          "io": "input",
          "name": "In L",
          "symbol": "in_l",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#right",
          "group": "http://calf.sourceforge.net/plugins/MultibandCompressor#in",
          "index": 1,
          "io": "input",
          "name": "In R",
          "symbol": "in_r",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#left",
          "group": "http://calf.sourceforge.net/plugins/MultibandCompressor#out",
          "index": 2,
          "io": "output",
          "name": "Out L",
          "symbol": "out_l",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#right",
          "group": "http://calf.sourceforge.net/plugins/MultibandCompressor#out",
          "index": 3,
          "io": "output",
          "name": "Out R",
          "symbol": "out_r",
          "type": "audio"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 4,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Bypass",
          "symbol": "bypass",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 5,
          "io": "input",
          "maximum": 64.0,
          "minimum": 0.015625,
          "name": "Input Gain",
          "symbol": "level_in",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 6,
          "io": "input",
          "maximum": 64.0,
          "minimum": 0.015625,
          "name": "Output Gain",
          "symbol": "level_out",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 7,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Meter-InL",
          "symbol": "meter_inL",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 8,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Meter-InR",
          "symbol": "meter_inR",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 9,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Meter-OutL",
          "symbol": "meter_outL",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 10,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Meter-OutR",
          "symbol": "meter_outR",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 11,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": 0.0,
          "symbol": "clip_inL",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 12,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": 0.0,
          "symbol": "clip_inR",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 13,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": 0.0,
          "symbol": "clip_outL",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 14,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": 0.0,
          "symbol": "clip_outR",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 120.0,
          "index": 15,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "Split 1/2",
          "symbol": "freq0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 3000.0,
          "index": 16,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "Split 2/3",
          "symbol": "freq1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 20000.0,
          "index": 17,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "Split 3/4",
          "symbol": "freq2",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 1.0,
          "entries": ([ /* 2 elements */
              0: "LR4",
              1: "LR8"
            ]),
          "index": 18,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Filter Mode",
          "symbol": "mode",
          "type": "enum"
        ]),
        ([ /* 8 elements */
          "default": 0.25,
          "index": 19,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.000977,
          "name": "Threshold 1",
          "symbol": "threshold0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 2.0,
          "index": 20,
          "io": "input",
          "maximum": 20.0,
          "minimum": 1.0,
          "name": "Ratio 1",
          "symbol": "ratio0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 150.0,
          "index": 21,
          "io": "input",
          "maximum": 2000.0,
          "minimum": 0.01,
          "name": "Attack 1",
          "symbol": "attack0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 300.0,
          "index": 22,
          "io": "input",
          "maximum": 2000.0,
          "minimum": 0.01,
          "name": "Release 1",
          "symbol": "release0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 23,
          "io": "input",
          "maximum": 64.0,
          "minimum": 1.0,
          "name": "Makeup 1",
          "symbol": "makeup0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 2.82843,
          "index": 24,
          "io": "input",
          "maximum": 8.0,
          "minimum": 1.0,
          "name": "Knee 1",
          "symbol": "knee0",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 0.0,
          "entries": ([ /* 2 elements */
              0: "RMS",
              1: "Peak"
            ]),
          "index": 25,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Detection 1",
          "symbol": "detection0",
          "type": "enum"
        ]),
        ([ /* 7 elements */
          "index": 26,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0625,
          "name": "Gain Reduction 1",
          "symbol": "compression0",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 27,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Output 1",
          "symbol": "output0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 28,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Bypass 1",
          "symbol": "bypass0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 29,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Solo 1",
          "symbol": "solo0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.25,
          "index": 30,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.000977,
          "name": "Threshold 2",
          "symbol": "threshold1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 2.0,
          "index": 31,
          "io": "input",
          "maximum": 20.0,
          "minimum": 1.0,
          "name": "Ratio 2",
          "symbol": "ratio1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 150.0,
          "index": 32,
          "io": "input",
          "maximum": 2000.0,
          "minimum": 0.01,
          "name": "Attack 2",
          "symbol": "attack1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 300.0,
          "index": 33,
          "io": "input",
          "maximum": 2000.0,
          "minimum": 0.01,
          "name": "Release 2",
          "symbol": "release1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 34,
          "io": "input",
          "maximum": 64.0,
          "minimum": 1.0,
          "name": "Makeup 2",
          "symbol": "makeup1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 2.82843,
          "index": 35,
          "io": "input",
          "maximum": 8.0,
          "minimum": 1.0,
          "name": "Knee 2",
          "symbol": "knee1",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 0.0,
          "entries": ([ /* 2 elements */
              0: "RMS",
              1: "Peak"
            ]),
          "index": 36,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Detection 2",
          "symbol": "detection1",
          "type": "enum"
        ]),
        ([ /* 7 elements */
          "index": 37,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0625,
          "name": "Gain Reduction 2",
          "symbol": "compression1",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 38,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Output 2",
          "symbol": "output1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 39,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Bypass 2",
          "symbol": "bypass1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 40,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Solo 2",
          "symbol": "solo1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.25,
          "index": 41,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.000977,
          "name": "Threshold 3",
          "symbol": "threshold2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 2.0,
          "index": 42,
          "io": "input",
          "maximum": 20.0,
          "minimum": 1.0,
          "name": "Ratio 3",
          "symbol": "ratio2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 150.0,
          "index": 43,
          "io": "input",
          "maximum": 2000.0,
          "minimum": 0.01,
          "name": "Attack 3",
          "symbol": "attack2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 300.0,
          "index": 44,
          "io": "input",
          "maximum": 2000.0,
          "minimum": 0.01,
          "name": "Release 3",
          "symbol": "release2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 45,
          "io": "input",
          "maximum": 64.0,
          "minimum": 1.0,
          "name": "Makeup 3",
          "symbol": "makeup2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 2.82843,
          "index": 46,
          "io": "input",
          "maximum": 8.0,
          "minimum": 1.0,
          "name": "Knee 3",
          "symbol": "knee2",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 0.0,
          "entries": ([ /* 2 elements */
              0: "RMS",
              1: "Peak"
            ]),
          "index": 47,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Detection 3",
          "symbol": "detection2",
          "type": "int"
        ]),
        ([ /* 7 elements */
          "index": 48,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0625,
          "name": "Gain Reduction 3",
          "symbol": "compression2",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 49,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Output 3",
          "symbol": "output2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 50,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Bypass 3",
          "symbol": "bypass2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 51,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Solo 3",
          "symbol": "solo2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.25,
          "index": 52,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.000977,
          "name": "Threshold 4",
          "symbol": "threshold3",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 2.0,
          "index": 53,
          "io": "input",
          "maximum": 20.0,
          "minimum": 1.0,
          "name": "Ratio 4",
          "symbol": "ratio3",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 150.0,
          "index": 54,
          "io": "input",
          "maximum": 2000.0,
          "minimum": 0.01,
          "name": "Attack 4",
          "symbol": "attack3",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 300.0,
          "index": 55,
          "io": "input",
          "maximum": 2000.0,
          "minimum": 0.01,
          "name": "Release 4",
          "symbol": "release3",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 56,
          "io": "input",
          "maximum": 64.0,
          "minimum": 1.0,
          "name": "Makeup 4",
          "symbol": "makeup3",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 2.82843,
          "index": 57,
          "io": "input",
          "maximum": 8.0,
          "minimum": 1.0,
          "name": "Knee 4",
          "symbol": "knee3",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 0.0,
          "entries": ([ /* 2 elements */
              0: "RMS",
              1: "Peak"
            ]),
          "index": 58,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Detection 4",
          "symbol": "detection3",
          "type": "int"
        ]),
        ([ /* 7 elements */
          "index": 59,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0625,
          "name": "Gain Reduction 4",
          "symbol": "compression3",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 60,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Output 4",
          "symbol": "output3",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 61,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Bypass 4",
          "symbol": "bypass3",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 62,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Solo 4",
          "symbol": "solo3",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 63,
          "io": "input",
          "maximum": 3.0,
          "minimum": 0.0,
          "name": "Notebook",
          "symbol": "notebook",
          "type": "int"
        ]),
        ([ /* 5 elements */
          "index": 64,
          "io": "input",
          "name": "Events",
          "symbol": "events_in",
          "type": "atom"
        ]),
        ([ /* 5 elements */
          "index": 65,
          "io": "output",
          "name": "Events",
          "symbol": "events_out",
          "type": "atom"
        ])
    }),
  "uri": "http://calf.sourceforge.net/plugins/MultibandCompressor"
]);
