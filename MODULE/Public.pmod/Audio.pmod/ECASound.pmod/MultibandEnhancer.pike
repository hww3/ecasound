inherit .Operation;

mapping(string:mixed) description = ([ /* 12 elements */
  "author": "Calf Studio Gear",
  "authoremail": "mailto:info@calf-studio-gear.org",
  "authorhomepage": "http://calf-studio-gear.org/",
  "binary": "file:///usr/local/lib/lv2/calf.lv2/calflv2gui.so",
  "bundle": "file:///usr/local/lib/lv2/calf.lv2/",
  "generic": "CalfMultibandEnhancer",
  "haslatency": 0,
  "name": "Calf Multiband Enhancer",
  "operator": "lv2",
  "origin": "MultibandEnhancer.lv2",
  "ports": ({ /* 37 elements */
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#left",
          "group": "http://calf.sourceforge.net/plugins/MultibandEnhancer#in",
          "index": 0,
          "io": "input",
          "name": "In L",
          "symbol": "in_l",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#right",
          "group": "http://calf.sourceforge.net/plugins/MultibandEnhancer#in",
          "index": 1,
          "io": "input",
          "name": "In R",
          "symbol": "in_r",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#left",
          "group": "http://calf.sourceforge.net/plugins/MultibandEnhancer#out",
          "index": 2,
          "io": "output",
          "name": "Out L",
          "symbol": "out_l",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#right",
          "group": "http://calf.sourceforge.net/plugins/MultibandEnhancer#out",
          "index": 3,
          "io": "output",
          "name": "Out R",
          "symbol": "out_r",
          "type": "audio"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 4,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Bypass",
          "symbol": "bypass",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 5,
          "io": "input",
          "maximum": 64.0,
          "minimum": 0.015625,
          "name": "Input Gain",
          "symbol": "level_in",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 6,
          "io": "input",
          "maximum": 64.0,
          "minimum": 0.015625,
          "name": "Output Gain",
          "symbol": "level_out",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 7,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Meter-InL",
          "symbol": "meter_inL",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 8,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Meter-InR",
          "symbol": "meter_inR",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 9,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Meter-OutL",
          "symbol": "meter_outL",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 10,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Meter-OutR",
          "symbol": "meter_outR",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 11,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": 0.0,
          "symbol": "clip_inL",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 12,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": 0.0,
          "symbol": "clip_inR",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 13,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": 0.0,
          "symbol": "clip_outL",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 14,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": 0.0,
          "symbol": "clip_outR",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 120.0,
          "index": 15,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "Split 1/2",
          "symbol": "freq0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 3000.0,
          "index": 16,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "Split 2/3",
          "symbol": "freq1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 20000.0,
          "index": 17,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "Split 3/4",
          "symbol": "freq2",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 1.0,
          "entries": ([ /* 2 elements */
              0: "LR4",
              1: "LR8"
            ]),
          "index": 18,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Filter Mode",
          "symbol": "mode",
          "type": "enum"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 19,
          "io": "input",
          "maximum": 1.0,
          "minimum": "-1.000000",
          "name": "Base 1",
          "symbol": "base0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 20,
          "io": "input",
          "maximum": 1.0,
          "minimum": "-1.000000",
          "name": "Base 2",
          "symbol": "base1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 21,
          "io": "input",
          "maximum": 1.0,
          "minimum": "-1.000000",
          "name": "Base 3",
          "symbol": "base2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 22,
          "io": "input",
          "maximum": 1.0,
          "minimum": "-1.000000",
          "name": "Base 4",
          "symbol": "base3",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 23,
          "io": "input",
          "maximum": 10.0,
          "minimum": 0.0,
          "name": "Harmonics 1",
          "symbol": "drive0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 24,
          "io": "input",
          "maximum": 10.0,
          "minimum": 0.0,
          "name": "Harmonics 2",
          "symbol": "drive1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 25,
          "io": "input",
          "maximum": 10.0,
          "minimum": 0.0,
          "name": "Harmonics 3",
          "symbol": "drive2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 26,
          "io": "input",
          "maximum": 10.0,
          "minimum": 0.0,
          "name": "Harmonics 4",
          "symbol": "drive3",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 27,
          "io": "input",
          "maximum": 10.0,
          "minimum": "-10.000000",
          "name": "Blend Harmonics 1",
          "symbol": "blend0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 28,
          "io": "input",
          "maximum": 10.0,
          "minimum": "-10.000000",
          "name": "Blend Harmonics 2",
          "symbol": "blend1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 29,
          "io": "input",
          "maximum": 10.0,
          "minimum": "-10.000000",
          "name": "Blend Harmonics 3",
          "symbol": "blend2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 30,
          "io": "input",
          "maximum": 10.0,
          "minimum": "-10.000000",
          "name": "Blend Harmonics 4",
          "symbol": "blend3",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 31,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Solo 1",
          "symbol": "solo0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 32,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Solo 2",
          "symbol": "solo1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 33,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Solo 3",
          "symbol": "solo2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 34,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Solo 4",
          "symbol": "solo3",
          "type": "float"
        ]),
        ([ /* 5 elements */
          "index": 35,
          "io": "input",
          "name": "Events",
          "symbol": "events_in",
          "type": "atom"
        ]),
        ([ /* 5 elements */
          "index": 36,
          "io": "output",
          "name": "Events",
          "symbol": "events_out",
          "type": "atom"
        ])
    }),
  "uri": "http://calf.sourceforge.net/plugins/MultibandEnhancer"
]);
