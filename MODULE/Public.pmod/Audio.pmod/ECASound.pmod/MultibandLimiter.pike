inherit .Operation;

mapping(string:mixed) description = ([ /* 12 elements */
  "author": "Calf Studio Gear",
  "authoremail": "mailto:info@calf-studio-gear.org",
  "authorhomepage": "http://calf-studio-gear.org/",
  "binary": "file:///usr/local/lib/lv2/calf.lv2/calflv2gui.so",
  "bundle": "file:///usr/local/lib/lv2/calf.lv2/",
  "generic": "CalfMultibandLimiter",
  "haslatency": 0,
  "name": "Calf Multiband Limiter",
  "operator": "lv2",
  "origin": "MultibandLimiter.lv2",
  "ports": ({ /* 49 elements */
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#left",
          "group": "http://calf.sourceforge.net/plugins/MultibandLimiter#in",
          "index": 0,
          "io": "input",
          "name": "In L",
          "symbol": "in_l",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#right",
          "group": "http://calf.sourceforge.net/plugins/MultibandLimiter#in",
          "index": 1,
          "io": "input",
          "name": "In R",
          "symbol": "in_r",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#left",
          "group": "http://calf.sourceforge.net/plugins/MultibandLimiter#out",
          "index": 2,
          "io": "output",
          "name": "Out L",
          "symbol": "out_l",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#right",
          "group": "http://calf.sourceforge.net/plugins/MultibandLimiter#out",
          "index": 3,
          "io": "output",
          "name": "Out R",
          "symbol": "out_r",
          "type": "audio"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 4,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Bypass",
          "symbol": "bypass",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 5,
          "io": "input",
          "maximum": 64.0,
          "minimum": 0.015625,
          "name": "Input Gain",
          "symbol": "level_in",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 6,
          "io": "input",
          "maximum": 64.0,
          "minimum": 0.015625,
          "name": "Output Gain",
          "symbol": "level_out",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 7,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Meter-InL",
          "symbol": "meter_inL",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 8,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Meter-InR",
          "symbol": "meter_inR",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 9,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Meter-OutL",
          "symbol": "meter_outL",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 10,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Meter-OutR",
          "symbol": "meter_outR",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 11,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": 0.0,
          "symbol": "clip_inL",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 12,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": 0.0,
          "symbol": "clip_inR",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 13,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": 0.0,
          "symbol": "clip_outL",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 14,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": 0.0,
          "symbol": "clip_outR",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 120.0,
          "index": 15,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "Split 1/2",
          "symbol": "freq0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 3000.0,
          "index": 16,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "Split 2/3",
          "symbol": "freq1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 20000.0,
          "index": 17,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "Split 3/4",
          "symbol": "freq2",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 1.0,
          "entries": ([ /* 2 elements */
              0: "LR4",
              1: "LR8"
            ]),
          "index": 18,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Filter Mode",
          "symbol": "mode",
          "type": "enum"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 19,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0625,
          "name": "Limit",
          "symbol": "limit",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 4.0,
          "index": 20,
          "io": "input",
          "maximum": 10.0,
          "minimum": 0.1,
          "name": "Lookahead",
          "symbol": "attack",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 30.0,
          "index": 21,
          "io": "input",
          "maximum": 1000.0,
          "minimum": 1.0,
          "name": "Release",
          "symbol": "release",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 22,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Min Release",
          "symbol": "minrel",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 23,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.125,
          "name": "Low",
          "symbol": "att0",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 24,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.125,
          "name": "LMid",
          "symbol": "att1",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 25,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.125,
          "name": "HMid",
          "symbol": "att2",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 26,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.125,
          "name": "Hi",
          "symbol": "att3",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 27,
          "io": "input",
          "maximum": 1.0,
          "minimum": "-1.000000",
          "name": "Weight 1",
          "symbol": "weight0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 28,
          "io": "input",
          "maximum": 1.0,
          "minimum": "-1.000000",
          "name": "Weight 2",
          "symbol": "weight1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 29,
          "io": "input",
          "maximum": 1.0,
          "minimum": "-1.000000",
          "name": "Weight 3",
          "symbol": "weight2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 30,
          "io": "input",
          "maximum": 1.0,
          "minimum": "-1.000000",
          "name": "Weight 4",
          "symbol": "weight3",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.5,
          "index": 31,
          "io": "input",
          "maximum": 1.0,
          "minimum": "-1.000000",
          "name": "Release 1",
          "symbol": "release0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.2,
          "index": 32,
          "io": "input",
          "maximum": 1.0,
          "minimum": "-1.000000",
          "name": "Release 2",
          "symbol": "release1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": "-0.200000",
          "index": 33,
          "io": "input",
          "maximum": 1.0,
          "minimum": "-1.000000",
          "name": "Release 3",
          "symbol": "release2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": "-0.500000",
          "index": 34,
          "io": "input",
          "maximum": 1.0,
          "minimum": "-1.000000",
          "name": "Release 4",
          "symbol": "release3",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 35,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Solo 1",
          "symbol": "solo0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 36,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Solo 2",
          "symbol": "solo1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 37,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Solo 3",
          "symbol": "solo2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 38,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Solo 4",
          "symbol": "solo3",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 39,
          "io": "output",
          "maximum": 1000.0,
          "minimum": 0.0,
          "name": "Effectively Release 1",
          "symbol": "effrelease0",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 40,
          "io": "output",
          "maximum": 1000.0,
          "minimum": 0.0,
          "name": "Effectively Release 2",
          "symbol": "effrelease1",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 41,
          "io": "output",
          "maximum": 1000.0,
          "minimum": 0.0,
          "name": "Effectively Release 3",
          "symbol": "effrelease2",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 42,
          "io": "output",
          "maximum": 1000.0,
          "minimum": 0.0,
          "name": "Effectively Release 4",
          "symbol": "effrelease3",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 43,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "ASC",
          "symbol": "asc",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 44,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "asc active",
          "symbol": "asc_led",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.5,
          "index": 45,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "ASC Level",
          "symbol": "asc_coeff",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 46,
          "io": "input",
          "maximum": 4.0,
          "minimum": 1.0,
          "name": "Oversampling",
          "symbol": "oversampling",
          "type": "int"
        ]),
        ([ /* 5 elements */
          "index": 47,
          "io": "input",
          "name": "Events",
          "symbol": "events_in",
          "type": "atom"
        ]),
        ([ /* 5 elements */
          "index": 48,
          "io": "output",
          "name": "Events",
          "symbol": "events_out",
          "type": "atom"
        ])
    }),
  "uri": "http://calf.sourceforge.net/plugins/MultibandLimiter"
]);
