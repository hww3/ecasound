mapping(string:mixed) description = ([]);
object client;
.Chain chain;
int chainop_id;
	
protected void create() {
    mapping d = description;
    
    d["_ports"] = d["ports"]; d["ports"] = ([]);
    int c = 1;
    foreach(d["_ports"];; mapping(string:mixed) p) {
        d["ports"][p["symbol"]] = p;
        if (p["type"] != "audio")
            p["id"] = c++;
    }
}


void enable(.Client _client, .Chain _chain, int chainop_id) {
    mapping d = description;
    this->chain = _chain;
	this->client = _client;
	this->chainop_id = chainop_id;
    
    string op = sprintf("-e%s:", d["operator"]);
    if (d["operator"] == "lv2") {
        op += d["uri"];
        foreach(d["_ports"];;mapping p) {
            if (p["type"] == "audio")
                continue;
            op += sprintf(",%O", p["default"]);
        }
    }
    client->send_command("cop-add " + op);
	client->send_command("cop-select " + chainop_id);
}

protected mixed _indices() {
	return ({"enable", "params"}) + make_func_list();
}

protected mixed `->(string identifier) {
	return `[](identifier);
}

protected mixed `[](string identifier) {
//	werror("`[](%O)", identifier);
	if(identifier == "enable") return enable;
	if(identifier == "params") return description["ports"];
	if(identifier == "get") return get_param;
	if(identifier == "set") return set_param;
	else if(has_prefix(identifier, "get_"))
	  return make_getter(identifier[4..]);
  	else if(has_prefix(identifier, "set_"))
  	  return make_setter(identifier[4..]);
}

array(string) make_func_list() {
	array x = ({});
    foreach(indices(description["ports"]);;string param) {
        x += ({"get_" + param, "set_" + param});
    }
	return x;
}

mixed get_param(string param) {
    mapping(string:mixed) p = description["ports"][param];
    if (zero_type(p)) {
        werror("Parameter %s doesn't exist\n", param);
        return 0;
    }
    //write("get %s (%d)\n", param, p["id"]);
	return client->send_copp_get_command(chain->setup->name, chain->name, chainop_id, p["id"]);
}

mixed set_param(string param, mixed value) {
    mapping(string:mixed) p = description["ports"][param];
    if (zero_type(p)) {
        werror("Parameter %s doesn't exist\n", param);
        return 0;
    }
    if (search(({"enum", "float", "int"}), p["type"]) >= 0) {
        value = limit(p["minimum"], value, p["maximum"]);
    }
    //write("set %s (%d): %O\n", param, p["id"], value);
	return client->send_copp_set_command(chain->setup->name, chain->name, chainop_id, p["id"], value);
}

function make_getter(string parameter_name) {
    mapping(string:mixed) p = description["ports"][parameter_name];
	string csname = chain->setup->name;
	string cname = chain->name;
	// if we are calling setters and getters, we assume (todo: check!) that ou chainsetup is the current chainsetup and active.
	return lambda() { return client->send_copp_get_command(csname, cname, chainop_id, p["id"]); };
}

function(string,mixed:mixed) make_setter(string parameter_name) {
    mapping(string:mixed) p = description["ports"][parameter_name];
	string csname = chain->setup->name;
	string cname = chain->name;
	// if we are calling setters and getters, we assume (todo: check!) that ou chainsetup is the current chainsetup and active.
	return lambda(mixed val) { client->send_copp_set_command(csname, cname, chainop_id, p["id"], val); };
}
