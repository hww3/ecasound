inherit .Operation;

array parameters = ({"in_l", "in_r", "out_l", "out_r",
	"base_freq",
	"mod_depth",
	"mod_rate",
	"feedback",
	"stages",
	"stereo",
	"reset",
	"amount",
	"dry",
	"on",
	"level_in",
	"level_out",
	"meter_inL",
	"meter_inR",
	"meter_outL",
	"meter_outR",
	"clip_inL",
	"clip_inR",
	"clip_outL",
	"clip_outR",
	"lfo",
	"events_in",
	"events_out",
	});

protected void () {
	::create("-elv2:http://calf.sourceforge.net/plugins/Phaser")
}

protected void do_enable() {

	// in_l
	// in_r
	// out_l
	// out_r
	// client->send_copp_set_command(chain->setup->name, chain->name, 0, chain->input_spec);
	// etc
}