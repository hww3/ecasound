inherit .Operation;

array(string) parameters = ({"in_l", "in_r", "out_l", "out_r",
	"bypass",
	"level_in",
	"level_out",
	"meter_inL",
	"meter_inR",
	"meter_outL",
	"meter_outR",
	"clip_inL",
	"clip_inR",
	"clip_outL",
	"clip_outR",
	"drone",
	"speed",
	"aging",
	"freq",
	"gain0",
	"pitch0",
	"active0",
	"gain1",
	"pitch1",
	"active1",
	"gain2",
	"pitch2",
	"active2",
	"gain3",
	"pitch3",
	"active3",
	"gain4",
	"pitch4",
	"active4",
	"gain5",
	"pitch5",
	"active5",
	"gain6",
	"pitch6",
	"active6",
	"events_in",
	"events_out",
	});

protected void create() {
	::create("-elv2:http://calf.sourceforge.net/plugins/Vinyl");
}

protected void do_enable() {

	// in_l
	// in_r
	// out_l
	// out_r
	// client->send_copp_set_command(chain->setup->name, chain->name, 0, chain->input_spec);
	// etc
}