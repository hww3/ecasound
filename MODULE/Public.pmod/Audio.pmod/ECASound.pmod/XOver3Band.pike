inherit .Operation;

mapping(string:mixed) description = ([ /* 12 elements */
  "author": "Calf Studio Gear",
  "authoremail": "mailto:info@calf-studio-gear.org",
  "authorhomepage": "http://calf-studio-gear.org/",
  "binary": "file:///usr/local/lib/lv2/calf.lv2/calflv2gui.so",
  "bundle": "file:///usr/local/lib/lv2/calf.lv2/",
  "generic": "CalfX-Over3Band",
  "haslatency": 0,
  "name": "Calf X-Over 3 Band",
  "operator": "lv2",
  "origin": "XOver3Band.lv2",
  "ports": ({ /* 34 elements */
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#left",
          "group": "http://calf.sourceforge.net/plugins/XOver3Band#in",
          "index": 0,
          "io": "input",
          "name": "In L",
          "symbol": "in_l",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#right",
          "group": "http://calf.sourceforge.net/plugins/XOver3Band#in",
          "index": 1,
          "io": "input",
          "name": "In R",
          "symbol": "in_r",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#left",
          "group": "http://calf.sourceforge.net/plugins/XOver3Band#out",
          "index": 2,
          "io": "output",
          "name": "Out L",
          "symbol": "out_l",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#right",
          "group": "http://calf.sourceforge.net/plugins/XOver3Band#out",
          "index": 3,
          "io": "output",
          "name": "Out R",
          "symbol": "out_r",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#left",
          "group": "http://calf.sourceforge.net/plugins/XOver3Band#out",
          "index": 4,
          "io": "output",
          "name": "Out L 2",
          "symbol": "out_l_2",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#right",
          "group": "http://calf.sourceforge.net/plugins/XOver3Band#out",
          "index": 5,
          "io": "output",
          "name": "Out R 2",
          "symbol": "out_r_2",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#left",
          "group": "http://calf.sourceforge.net/plugins/XOver3Band#out",
          "index": 6,
          "io": "output",
          "name": "Out L 3",
          "symbol": "out_l_3",
          "type": "audio"
        ]),
        ([ /* 7 elements */
          "designation": "http://lv2plug.in/ns/ext/port-groups#right",
          "group": "http://calf.sourceforge.net/plugins/XOver3Band#out",
          "index": 7,
          "io": "output",
          "name": "Out R 3",
          "symbol": "out_r_3",
          "type": "audio"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 8,
          "io": "input",
          "maximum": 64.0,
          "minimum": 0.015625,
          "name": "Gain",
          "symbol": "level",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 9,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Input L",
          "symbol": "meter_L",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 10,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Input R",
          "symbol": "meter_R",
          "type": "float"
        ]),
        ([ /* 9 elements */
          "default": 2.0,
          "entries": ([ /* 3 elements */
              0: "LR2",
              1: "LR4",
              2: "LR8"
            ]),
          "index": 11,
          "io": "input",
          "maximum": 2.0,
          "minimum": 0.0,
          "name": "Filter Mode",
          "symbol": "mode",
          "type": "enum"
        ]),
        ([ /* 8 elements */
          "default": 120.0,
          "index": 12,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "Transition 1",
          "symbol": "freq0",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 3000.0,
          "index": 13,
          "io": "input",
          "maximum": 20000.0,
          "minimum": 10.0,
          "name": "Transition 2",
          "symbol": "freq1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 14,
          "io": "input",
          "maximum": 64.0,
          "minimum": 0.015625,
          "name": "Gain 1",
          "symbol": "level1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 15,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Active 1",
          "symbol": "active1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 16,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Phase 1",
          "symbol": "phase1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 17,
          "io": "input",
          "maximum": 20.0,
          "minimum": 0.0,
          "name": "Delay 1",
          "symbol": "delay1",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 18,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Level L 1",
          "symbol": "meter_L1",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 19,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Level R 1",
          "symbol": "meter_R1",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 20,
          "io": "input",
          "maximum": 64.0,
          "minimum": 0.015625,
          "name": "Gain 2",
          "symbol": "level2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 21,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Active 2",
          "symbol": "active2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 22,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Phase 2",
          "symbol": "phase2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 23,
          "io": "input",
          "maximum": 20.0,
          "minimum": 0.0,
          "name": "Delay 2",
          "symbol": "delay2",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 24,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Level L 2",
          "symbol": "meter_L2",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 25,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Level R 2",
          "symbol": "meter_R2",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 26,
          "io": "input",
          "maximum": 64.0,
          "minimum": 0.015625,
          "name": "Gain 3",
          "symbol": "level3",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 1.0,
          "index": 27,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Active 3",
          "symbol": "active3",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 28,
          "io": "input",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Phase 3",
          "symbol": "phase3",
          "type": "float"
        ]),
        ([ /* 8 elements */
          "default": 0.0,
          "index": 29,
          "io": "input",
          "maximum": 20.0,
          "minimum": 0.0,
          "name": "Delay 3",
          "symbol": "delay3",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 30,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Level L 3",
          "symbol": "meter_L3",
          "type": "float"
        ]),
        ([ /* 7 elements */
          "index": 31,
          "io": "output",
          "maximum": 1.0,
          "minimum": 0.0,
          "name": "Level R 3",
          "symbol": "meter_R3",
          "type": "float"
        ]),
        ([ /* 5 elements */
          "index": 32,
          "io": "input",
          "name": "Events",
          "symbol": "events_in",
          "type": "atom"
        ]),
        ([ /* 5 elements */
          "index": 33,
          "io": "output",
          "name": "Events",
          "symbol": "events_out",
          "type": "atom"
        ])
    }),
  "uri": "http://calf.sourceforge.net/plugins/XOver3Band"
]);
