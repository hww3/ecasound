import Public.Audio.ECASound;

client c;
Chainsetup cs;
Chain chain;

Operation amplifier;
Operation equalizer;
Operation enhancer;
Operation compressor;
Operation limiter;
Operation xover;

int main() {

  c = client("ecasound");

	// setup and chain names are somewhat arbitrary.
	// a longer term goal would be to allow multiple setups to be present at once and selectable by name.
	// you add a chain to the setup per set of channels. by default there are 2 channels for stereo, etc.
  cs = Chainsetup("play_chainsetup");
  chain = Chain("1st_chain");
  
  // jack runs at 48khz, and the sample audio file is only 44.1khz, so we need to resample.
  chain->set_input_format("16,2,48000");
  
  // input can be a path to an audio file, a jack or alsa specification, or can be null
  // input and output can be changed at any time, though it will cause the engine to restart; this will result in
  // gaps in output.
  chain->set_input("a2002011001-e02.wav");
  chain->set_output("jack,system");
  
  // each plugin is represented by an operation object. You create an operation, and then add it to a chain.
  // its ports _should_ be automatically connected to the operations (or inputs and outputs) before and after
  
  equalizer = Equalizer8Band();
  chain->add_operation(equalizer);
  
  enhancer = MultibandEnhancer();
  chain->add_operation(enhancer);

  compressor = MultibandCompressor();
  chain->add_operation(compressor);

  limiter = MultibandLimiter();
  chain->add_operation(limiter);
  
  // SwhAmplifer operation uses an LV2 plugin from the SWH plugins collection as a test basis.
  // Amplifier operation uses internal ecasound functionality.
  amplifier = AmplifierOperation(100);
  chain->add_operation(amplifier);

// think this requires enough output ports to use; re-enable on devices that have enough. 
/*  xover = XOver3Band();
  chain->add_operation(xover);
*/
  // then we add the chain to the setup and enable it.
  cs->add_chain(chain);
  c->enable_chainsetup(cs);
  
  // on my mac, i needed to set the position; this may not be universally necessary.
  c->set_position(0.0);	
  
  // then we start playing.
  c->start();
  
  // parameters in each operation are accessed by name.
  float f = amplifier->get_level();
  // or
  f = amplifier->get("level");

  amplifier->set_level(50.0);
  // or
  amplifier->set("level", 50.0);

  sleep(3);
  //while(c->engine_status() == "running") sleep(0.1);
  
 
  return -1; 
}
