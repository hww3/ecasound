object c;

int main()
{
  call_out(setup, 1);
  return -1;
}

void setup(){
 c = Public.Audio.ECASound.client();
 call_out(begin, 1);
}

void begin() {
//  c->send_command("cs-remove");  
c->send_command("stop");
  c->send_command("cs-add play_chainsetup");
  c->send_command("c-add 1st_chain");
  c->send_command("ai-add a2002011001-e02.wav");
  c->send_command("ao-add jack,system");
//  c->send_command("ai-attach");
//  c->send_command("ao-attach");

/*  
  c->send_command("cop-add -ea:100");
  c->send_command("cop-select 1");
  c->send_command("copp-select 1");
*/
//  c->send_command("cs-select play_chainsetup");  
//  c->send_command("cs-connect");
//  c->send_command("start");
//  c->send_command("get-position");
//  c->send_command("cop-status");
//  c->send_command("cs");
//  c->send_command("cs-status");
//  c->send_command("engine-status");
//  c->send_command("jack-list-connections");
//  c->send_command("jack-connect ecasound:out_1 system:playback_1");
//  c->send_command("jack-connect ecasound:out_2 system:playback_2");
  
  call_out(run, 5);
}

void run() {
  c->send_command("set-position 0.0");	
  c->send_command("start");
  call_out(check, 1);
}

void check() {
  c->send_command("engine-status");
  string l = c->last_string();
  write("last: %O\n", l);
  if(l != "running") exit(0);
  c->send_command("get-position");
  float f = c->last_float();
  werror("position: %O\n", f);
  /*
  if(f > 5) {
    c->send_command("copp-get");
    f = c->last_float();
	if((f-2) > 0.0)
      c->send_command("copp-set " + (f-2));
  }
*/
  call_out(check, 0.1);
}

void finish() {
  c->send_command("stop");
  c->send_command("cs-disconnect");
  c->send_command("cop-status");
  exit(0);
}
