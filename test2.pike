import Public.Audio.ECASound;

object c;
object op;

int main()
{
  call_out(setup, 1);
  return -1;
}

void setup(){
 c = client();
 call_out(begin, 1);
}

void begin() {

	// setup and chain names are somewhat arbitrary.
	// a longer term goal would be to allow multiple setups to be present at once and selectable by name.
	// you add a chain to the setup per set of channels. by default there are 2 channels for stereo, etc.
  Chainsetup cs = Chainsetup("play_chainsetup");
  Chain chain = Chain("1st_chain");
  
  // jack runs at 48khz, and the sample audio file is only 44.1khz, so we need to resample.
  chain->set_input_format("16,2,48000");
  chain->set_input("a2002011001-e02.wav");
  chain->set_output("jack,system");
  op = AmplifierOperation(100);
  chain->add_operation(op);  
  cs->add_chain(chain);
  c->enable_chainsetup(cs);
    
  call_out(run, 5);
}

void run() {
  // required for audio files, probably not for live data.
  c->send_command("set-position 0.0");	
  c->start();
  call_out(check, 1);
}

int ch = -2;


// so, this is a demo of getting and setting parameters.
// while the engine is running (that is, the file hasn't played through):
//  we get the current position, and if it's more than 5 seconds in,
//  we change the amplification level by an increment of what it currently is.
void check() {
  c->send_command("engine-status");
  string l = c->last_string();
  write("last: %O\n", l);
  if(l != "running") exit(0);
  c->send_command("get-position");
  float f = c->last_float();
  werror("position: %O\n", f);
  if(f > 5) {
    f = op->get_level();
  werror("level: %O\n", f);
    if(f < 0.1)
      ch = 2;
    if( f > 99)
	  ch = -2;
    op->set_level(f+ch); // level is a parameter defined in AmplifierPlugin.
	  
  }

  call_out(check, 0.1);
}

void finish() {
  c->stop();
  exit(0);
}
